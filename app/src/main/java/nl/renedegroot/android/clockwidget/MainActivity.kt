package nl.renedegroot.android.clockwidget

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import nl.renedegroot.android.clockwidget.clock.ClockFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                add(R.id.first, ClockFragment(), "clock")
            }
        }
    }
}