package nl.renedegroot.android.clockwidget.clock

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import nl.renedegroot.android.clockwidget.clock.internal.Clock
import nl.renedegroot.android.clockwidget.clock.theme.ClockWidgetTheme
import java.time.LocalTime

class ClockFragment : Fragment() {

    private val digits = MutableStateFlow(listOf('1', '2', ':', '4', '8'))
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) =
        ComposeView(requireContext()).apply {
            setContent {
                // A surface container using the 'background' color from the theme
                Surface(
                    color = MaterialTheme.colorScheme.background
                ) {
                    Box(
                        contentAlignment = Alignment.Center
                    ) {
                        val digits by digits.collectAsState()
                        Clock(digits)
                    }
                }
            }
        }

    override fun onResume() {
        super.onResume()
        viewLifecycleOwner.lifecycleScope.launch(context = Dispatchers.Default) {
            while (true) {
                val time = LocalTime.now()
                digits.emit(
                    listOf(
                        time.hour.secondDigit(),
                        time.hour.firstDigit(),
                        ':',
                        time.minute.secondDigit(),
                        time.minute.firstDigit(),
                        ':',
                        time.second.secondDigit(),
                        time.second.firstDigit()
                    )
                )
                delay(100L)
            }
        }
    }

    private fun Int.firstDigit() = (this % 10).toString().toCharArray().first()
    private fun Int.secondDigit() = (this / 10).toString().toCharArray().first()
}
