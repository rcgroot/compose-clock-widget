package nl.renedegroot.android.clockwidget.clock.internal

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import nl.renedegroot.android.clockwidget.clock.R
import nl.renedegroot.android.clockwidget.clock.theme.ClockWidgetTheme


@Preview(showBackground = true)
@Composable
private fun ClockPreview() {
    ClockWidgetTheme {
        Clock(listOf('1', '2', ':', '3', '4', '5', '6', '7', '8', '9', '0'))
    }
}

@Composable
internal fun Clock(
    digits: List<Char>,
    modifier: Modifier = Modifier
) {
    Box(
        modifier = modifier
            .clip(RoundedCornerShape(32.dp))
            .border(
                4.dp,
                Color.Gray,
                shape = RoundedCornerShape(32.dp),
            )
            .background(Color.Black)

    ) {
        Row(
            modifier = modifier
                .padding(start = 64.dp, end = 64.dp, top = 32.dp, bottom = 32.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            for (digit in digits) {
                Digit(
                    modifier = Modifier.weight(
                        if (digit.isDigit()) {
                            1F
                        } else {
                            0.3F
                        }
                    ),
                    digit = digit
                )
            }
        }
    }
}

@Preview
@Composable
private fun Digit() {
    Box {
        Digit(digit = '8')
    }
}

@Composable
private fun Digit(modifier: Modifier = Modifier, digit: Char) {
    val enter = fadeIn()
    val exit = fadeOut()
    Box(modifier = modifier.padding(4.dp)) {
        if (!digit.isDigit()) {
            Image(
                painter = painterResource(id = R.drawable.digit_art_dots),
                contentDescription = null
            )
        }
        AnimatedVisibility(
            modifier = Modifier.align(Alignment.Center),
            visible = digit.hasVisibleCenter(),
            enter = enter,
            exit = exit
        ) {
            Image(
                painter = painterResource(id = R.drawable.digit_art_middle),
                contentDescription = null
            )
        }
        Column(
            modifier = Modifier.align(Alignment.Center)
        ) {
            Box {
                androidx.compose.animation.AnimatedVisibility(
                    visible = digit.hasVisibleTop(),
                    enter = enter,
                    exit = exit
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.digit_art_top),
                        contentDescription = null
                    )
                }
                androidx.compose.animation.AnimatedVisibility(
                    visible = digit.hasVisibleLeftTop(),
                    enter = enter,
                    exit = exit
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.digit_art_left_top),
                        contentDescription = null
                    )
                }
                androidx.compose.animation.AnimatedVisibility(
                    modifier = Modifier.align(Alignment.TopEnd),
                    visible = digit.hasVisibleRightTop(),
                    enter = enter,
                    exit = exit
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.digit_art_right_top),
                        contentDescription = null
                    )
                }
            }
            Box {
                androidx.compose.animation.AnimatedVisibility(
                    modifier = Modifier.align(Alignment.CenterStart),
                    visible = digit.hasVisibleLeftBottom(),
                    enter = enter,
                    exit = exit
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.digit_art_left_bottom),
                        contentDescription = null
                    )
                }
                androidx.compose.animation.AnimatedVisibility(
                    modifier = Modifier.align(Alignment.CenterEnd),
                    visible = digit.hasVisibleRightBottom(),
                    enter = enter,
                    exit = exit
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.digit_art_right_bottom),
                        contentDescription = null
                    )
                }
                androidx.compose.animation.AnimatedVisibility(
                    modifier = Modifier.align(Alignment.BottomCenter),
                    visible = digit.hasVisibleBottom(),
                    enter = enter,
                    exit = exit
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.digit_art_bottom),
                        contentDescription = null
                    )
                }
            }
        }
    }
}

private fun Char.hasVisibleCenter() =
    this == '2' || this == '3' || this == '4' || this == '5' || this == '6' || this == '8' || this == '9'

private fun Char.hasVisibleBottom() =
    this == '0' || this == '2' || this == '3' || this == '5' || this == '6' || this == '8' || this == '9'

private fun Char.hasVisibleTop() =
    this == '0' || this == '2' || this == '3' || this == '5' || this == '6' || this == '7' || this == '8' || this == '9'

private fun Char.hasVisibleLeftTop() =
    this == '0' || this == '4' || this == '5' || this == '6' || this == '8' || this == '9'

private fun Char.hasVisibleLeftBottom() =
    this == '0' || this == '2' || this == '6' || this == '8'

private fun Char.hasVisibleRightTop() =
    this == '0' || this == '1' || this == '2' || this == '3' || this == '4' || this == '7' || this == '8' || this == '9'

private fun Char.hasVisibleRightBottom() =
    this == '0' || this == '1' || this == '3' || this == '4' || this == '5' || this == '6' || this == '7' || this == '8' || this == '9'
